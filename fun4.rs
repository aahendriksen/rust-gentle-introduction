// fun4.rs

fn fill_(x : &mut f64, val : f64){
    *x = val;
}

fn main() {
    let mut res = 0.0;
    fill_(&mut res, 2.0);
    println!("res is {}", res);
}
