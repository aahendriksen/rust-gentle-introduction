fn by_ref(x : &i32) -> i32 {
    *x + 1
}

fn main() {
    let x = 1;
    println!("by_ref = {}", by_ref(&x));
}
