fn sqr(x : f64) -> f64 {
    x * x
}

fn abs(x : f64) -> f64 {
    if x < 0.0 {
        -x
    } else {
        x
    }
}

fn clamp(x : f64, low : f64, high : f64) -> f64 {
    if x < low {
        low
    } else if high < x {
        high
    } else {
        x
    }
}

fn factorial(n : u64) -> u64 {
    if n == 1 {
        1
    } else {
        n * factorial(n-1)
    }
}

fn main() {
    let res = sqr(2.0);
    let mut x = 3.0;
    let a = 2.0;
    let b = 4.0;
    println!("Res is {}", res);
    println!("Res abs {} {}", abs(-res), abs(res));
    println!("Clamp({}, {}, {}) = {}", x ,a, b, clamp(x, a, b));
    x = 0.0;
    println!("Clamp({}, {}, {}) = {}", x ,a, b, clamp(x, a, b));
    x = 100.0;
    println!("Clamp({}, {}, {}) = {}", x ,a, b, clamp(x, a, b));
    println!("factorial({}) = {}", 5, factorial(5));
}
