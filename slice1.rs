// slice1.rs
fn main() {
    let ints = [1,2,3,4,5,6];
    // let ints = 1..7; // This does not work. (std::ops::Range<{Integer}>)
    let slice1 = &ints[0..2];
    let slice2 = &ints[1..];

    println!("ints {:?}", ints);
    println!("slice1 {:?}", slice1);
    println!("slice2 {:?}", slice2);
}


// Next:
// https://stevedonovan.github.io/rust-gentle-intro/1-basics.html#optional-values
