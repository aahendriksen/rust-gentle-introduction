// array2.rs

fn sum(values : &[i32]) -> i32 {
    let mut sum = 0;
    for v in values.iter() {
        sum += v;
    }
    sum
}

fn main() {
    let arr = [10, 20, 30, 40];
    let res = sum(&arr);
    println!("sum {}", res);
}
